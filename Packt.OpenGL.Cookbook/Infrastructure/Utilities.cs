﻿using System;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.CodeDom;

namespace Packt.OpenGL.Cookbook.Infrastructure
{
    public static class Utilities
    {
        public static int GetSizeOf<T>(T[] array) {
            int sizeOfSingleElement = Marshal.SizeOf(default(T));
            int sizeOfArray = sizeOfSingleElement * array.Length;
            return sizeOfArray;
        }
    }
}
