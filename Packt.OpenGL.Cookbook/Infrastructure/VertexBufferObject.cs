﻿using System;
using OpenTK.Graphics.OpenGL4;
using System.Collections.Generic;

namespace Packt.OpenGL.Cookbook.Infrastructure
{

    public class VertexAttributeDetails {
        public int AttributeId { get; set; }
        public int AttributeSize { get; set; }
        public VertexAttribPointerType AttributeType { get; set; }
        public bool IsNormalised { get; set; }
        public int Stride { get; set; }
        public int OffsetToAttributeData { get; set; }
    }

    public class VertexBufferObject<T> where T : struct
    {
        private int _vboId;
        private BufferTarget _bufferTarget;
        private BufferUsageHint _hint;
        private T[] _bufferData;

        public VertexBufferObject(BufferTarget bufferTarget, BufferUsageHint hint , T[] bufferData)
        {
            _bufferTarget = bufferTarget;
            _hint = hint;
            _bufferData = bufferData;

            GenerateBufferWithOpenGL();
        }

        public void GenerateBufferWithOpenGL()
        {
            GL.GenBuffers(1, out _vboId);
        }

        public void UnloadBuffer()
        {
            GL.DeleteBuffers(1, ref _vboId);
        }

        public void BufferData()
        {
            // glBindBuffer (GL_ARRAY_BUFFER, vboVerticesID);
            GL.BindBuffer(_bufferTarget, _vboId);
            {
                //glBufferData (GL_ARRAY_BUFFER, sizeof(vertices), &vertices[0], GL_STATIC_DRAW);
                GL.BufferData(_bufferTarget, Utilities.GetSizeOf(_bufferData), _bufferData, _hint);
            }
        }

        public void BufferVertexAttribPointerData(IEnumerable<VertexAttributeDetails> details) {
            foreach (var d in details)
            {
                BufferVertexAttribPointerData(d);
            }
        }

        public void BufferVertexAttribPointerData(VertexAttributeDetails details)
        {
            //glEnableVertexAttribArray(shader["vVertex"]);
            GL.EnableVertexAttribArray(details.AttributeId);

            //glVertexAttribPointer(shader["vVertex"], 3, GL_FLOAT, GL_FALSE, 0, 0);
            GL.VertexAttribPointer(details.AttributeId, details.AttributeSize,
                                   details.AttributeType, details.IsNormalised, details.Stride, details.OffsetToAttributeData);
        }

    }
}
