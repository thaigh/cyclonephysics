﻿using System;
using System.Collections;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL4;
using System.IO;

namespace Packt.OpenGL.Cookbook.Infrastructure
{
    /// <summary>
    /// Represents a Open GL Shader Language (GLSL) shader program.
    /// Used for loading, binding, and unbinding shader programs at runtime and
    /// storing Attribute / Uniform data in memory, rather than fetching from hardware
    /// 
    /// A shader program can have up to three shaders registered.
    /// </summary>
    public class GLSLShaderProgram
    {
        //public int RegisteredShaders { get; private set; }
        private int[] _registeredShaders = new int[Enum.GetNames(typeof(ShaderType)).Length];

        // Attributes may change over the course of shader execution (Vertices change from Vertex -> Geometry stages)
        private Dictionary<string, int> _attributeLocations = new Dictionary<string, int>();

        // Uniforms are constant throughout the shader execution pipeline (Texture samplers)
        private Dictionary<string, int> _uniformLocations = new Dictionary<string, int> ();
        private int _programId;

        public GLSLShaderProgram () {
            //_registeredShaders = new int[Enum.GetNames(typeof(ShaderType)).Length];
            //_attributeLocations = new Dictionary<string, int>();
            //_uniformLocations = new Dictionary<string, int> ();
        }

        public void LoadFromString(ShaderType shaderType, string shaderSourceCode) {
            // Responsible for:
            // - glCreateShader
            // - glShaderSource
            // - glCompileShader
            // - glGetShaderInfoLog

            // Create the Shader
            int shaderProgramId = GL.CreateShader (shaderType);

            // Load the shader from source code
            GL.ShaderSource (shaderProgramId, shaderSourceCode);

            // Compile Shader
            CompileShader (shaderProgramId);

            _registeredShaders[GetShaderIndex(shaderType)] = shaderProgramId;
            //RegisteredShaders++;
        }


        private int GetShaderIndex(ShaderType shaderType) {
            switch (shaderType) {
                case ShaderType.VertexShader: return 0;
                case ShaderType.FragmentShader: return 1;
                case ShaderType.GeometryShader: return 2;
            }

            return -1;
        }

        private void CompileShader(int shaderProgramId) {
            // Compile the shader
            GL.CompileShader (shaderProgramId);

            // Check the compilation status
            int shaderStatus;
            GL.GetShader (shaderProgramId, ShaderParameter.CompileStatus, out shaderStatus);
            if (0 == shaderStatus) {
                //int shaderLogLength;
                //GL.GetShader (shaderProgramId, ShaderParameter.InfoLogLength, out shaderLogLength);

                string shaderLog;
                GL.GetShaderInfoLog (shaderProgramId, out shaderLog);
                throw new Exception (String.Format("Shader Compile Error:\n{0}", shaderLog));
            }
        }

        public void LoadFromFile(ShaderType shaderType, string shaderFilePath) {
            string s = File.ReadAllText(shaderFilePath);
            LoadFromString (shaderType, s);
        }

        public void CreateAndLink() {
            // Responsible for:
            // - glCreateProgram
            // - glAttachShader
            // - glLinkProgram
            // - glGetProgramInfoLog

            // Create Program
            _programId = GL.CreateProgram ();

            // Attach Sahders
            AttachShaders();

            LinkShaders ();
            DeleteShaders ();
        }

        private void AttachShader(ShaderType shaderType) {
            if (_registeredShaders [GetShaderIndex(shaderType)] != 0)
                GL.AttachShader (_programId, _registeredShaders [GetShaderIndex(shaderType)]);
        }

        private void LinkShaders() {
            GL.LinkProgram (_programId);

            int linkStatus;
            GL.GetProgram (_programId, GetProgramParameterName.LinkStatus, out linkStatus);
            if (0 == linkStatus) {
                string programLog;
                GL.GetProgramInfoLog (_programId, out programLog);
                throw new Exception (String.Format("Error linking Shader Program: {1}", programLog));
            }
        }

        private void AttachShaders() {
            AttachShader (ShaderType.VertexShader);
            AttachShader (ShaderType.FragmentShader);
            AttachShader (ShaderType.GeometryShader);
        }

        private void DeleteShaders() {
            GL.DeleteShader ((int)ShaderType.VertexShader);
            GL.DeleteShader ((int)ShaderType.FragmentShader);
            GL.DeleteShader ((int)ShaderType.GeometryShader);
        }

        public void Use () { GL.UseProgram (_programId); }
        public void Unuse () { GL.UseProgram (0); }
        public void Delete () { GL.DeleteProgram (_programId); }

        public void AddAttribute (string attribute) {
            _attributeLocations.Add (attribute, GL.GetAttribLocation (_programId, attribute));
        }

        public void AddUniform (string uniform) {
            _uniformLocations.Add (uniform, GL.GetUniformLocation (_programId, uniform));
        }

        public int GetAttribute (string attribute) { return _attributeLocations[attribute]; }
        public int GetUniform (string uniform) { return _uniformLocations[uniform]; }
    }


}

