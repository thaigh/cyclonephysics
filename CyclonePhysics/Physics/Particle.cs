﻿using System;
using CyclonePhysics.Math;

namespace CyclonePhysics
{
    /// <summary>
    /// A Particle is the simplest object that can be simulated in a physics system.
    /// It represents a point-mass - an object with mass, but no size
    /// </summary>
    public class Particle
    {
        public Vector3 Position { get; private set; }
        public Vector3 Velocity { get; private set; }
        public Vector3 Accelleration { get; set; }

        /// <summary>
        /// Holds the amount of damping applied to linear motion.
        /// Damping is required to remove energy added through numerican instability
        /// during integration
        /// </summary>
        public float Damping { get; set; }

        /// <summary>
        /// Rather than storing the mass (and risk 0 mass or infinite mass objects)
        /// we store the Inverse mass (1/m). Infinite mass objects are defined as InverseMass=0
        /// and zero mass objects are undefined
        /// </summary>
        private float InverseMass { get; set; }
        public float Mass {
            get { return 1.0f / InverseMass; }
            set { SetMass (value); }
        }


        public Particle ()
        {
        }

        public void SetMass(float mass) {
            if (mass == 0)
                throw new ArgumentException ("Particle masses must have positive mass");
            InverseMass = 1.0f / mass;
        }

        public void SetInfiniteMass() { InverseMass = 0; }

        public void IntegratePosition(float deltaTime) {

            // Check than time is positive
            if (deltaTime <= 0)
                throw new ArgumentException ("Delta time must be positive to apply integration");

            // We cannot apply forces on Infinite Masses
            if (InverseMass == 0) return;

            // Position Update: p' = p + vt
            Position = Position.AddScaledVector(Velocity, deltaTime);

            // Calculate the accelleration as impacted by forces applied
            Vector3 calcAccelleration = ApplyAccellerationForces();

            // Velocity Update: v' = vd^t + at
            // Velocity = Velocity.AddScaledVector (calcAccelleration, deltaTime);
            // Velocity *= Math.Pow (Damping, deltaTime);
            Vector3 dampennedVelocity = Velocity * (float)System.Math.Pow (Damping, deltaTime);
            Vector3 accellerationUpdate = calcAccelleration * deltaTime;
            Velocity = dampennedVelocity + accellerationUpdate;

            ClearForces ();

        }

        private Vector3 ApplyAccellerationForces() {
            // TODO: Implement calculation of forces acting on accelleration
            return Accelleration;
        }

        private void ClearForces() {
            // TODO: Implement clearing of Forces
        }
    }
}

