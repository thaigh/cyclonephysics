﻿using System;

namespace CyclonePhysics.Math
{
    /// <summary>
    /// Holds a 3 Dimensional Vector
    /// </summary>
    public class Vector3
    {

        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public float Length { get { return Magnitude (); } }

        public Vector3 () { X = 0; Y = 0; Z = 0;}
        public Vector3 (float x, float y, float z) { X = x; Y = y; Z = z;}

        private void FromVector(Vector3 vec) {
            X = vec.X; Y = vec.Y; Z = vec.Z;
        }

        /// <summary>
        /// Flips all components of this vector
        /// </summary>
        public Vector3 Inverted() { return Multiply (-1); }
        public void Invert() { FromVector (Inverted ()); }

        public float SquaredLength() {
            return (X * X) + (Y * Y) + (Z * Z);
        }

        public float Magnitude() {
            return (float)System.Math.Sqrt (SquaredLength());
        }

        public void Normalise() { FromVector (Normalised ()); }
        public Vector3 Normalised() {
            float magnitude = Magnitude ();
            if (magnitude > 0)
                return Divide (magnitude);
            return this;
        }

        public void DivideUpdate(float scalar) { FromVector (Divide (scalar)); }
        public Vector3 Divide(float scalar) {
            return Multiply (((float)1) / scalar); 
        }

        public void MultiplyUpdate(float scalar) { FromVector (Multiply (scalar)); }
        public Vector3 Multiply(float scalar) {
            return new Vector3(
                X *= scalar,
                Y *= scalar,
                Y *= scalar
            );
        }

        public void AddUpdate(Vector3 vec) { FromVector (Add (vec)); }
        public Vector3 Add(Vector3 vec) {
            return new Vector3 (X + vec.X, Y + vec.Y, Z + vec.Z);
        }

        public void SubtractUpdate(Vector3 vec) { FromVector (Subtract (vec)); }
        public Vector3 Subtract(Vector3 vec) {
            return Add (vec.Inverted ());
        }

        public Vector3 AddScaledVector(Vector3 vec, float scalar) {
            return Add (vec.Multiply(scalar));
        }

        public Vector3 ComponentProduct(Vector3 vec) {
            return new Vector3 (X * vec.X, Y * vec.Y, Z * vec.Z);
        }

        public float DotProduct(Vector3 vec) {
            return (X * vec.X) + (Y * vec.Y) + (Z * vec.Z);
        }

        public Vector3 CrossProduct(Vector3 vec) {
            return new Vector3(
                Y * vec.Z - Z * vec.Y,
                X * vec.Z - Z * vec.X,
                X * vec.Y - Y * vec.X
            );
        }

        public static void MakeOrthonormalBasis(ref Vector3 v1, ref Vector3 v2, ref Vector3 v3) {
            v1.Normalise ();
            v3 = v1.CrossProduct (v2);

            if (v3.Magnitude() == 0)
                return;

            v3.Normalise ();
            v2 = v3.CrossProduct (v1);
        }

        public static Vector3 operator * (Vector3 vec, float scalar) { return vec.Multiply(scalar); }
        public static Vector3 operator + (Vector3 vec, Vector3 vec2) { return vec.Add (vec2); }
        public static float operator * (Vector3 vec, Vector3 vec2) { return vec.DotProduct(vec2); }
    }
}

