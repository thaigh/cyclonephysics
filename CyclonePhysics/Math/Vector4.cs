﻿using System;

namespace CyclonePhysics.Math
{
    /// <summary>
    /// Represents a vector in 4-Dimensional Space using Homogeneous coordinate W
    /// Can also be used to represent an RGB colour with Alpha channel
    /// </summary>
    public class Vector4 : Vector3
    {
        public float W { get; set; }

        public Vector4 () : base() { W = 0; }
        public Vector4(float x, float y, float z, float w) : base(x,y,z)
        {
            W = w;
        }
    }
}

