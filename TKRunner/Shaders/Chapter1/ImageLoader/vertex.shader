﻿#version 330 core

// Inputs from Application
layout(location=0) in vec2 vVertex; // Object space vertex

// Outputs from vertex shader to fragment Shader
smooth out vec2 vUV; // Texture coordinates for texture lookup in the fragment shader

void main()
{    
    // Output the clipspace position
    gl_Position = vec4(vVertex*2.0-1, 0, 1);     

    // Set the input object space vertex position as texture coordinate
    vUV = vVertex;
}