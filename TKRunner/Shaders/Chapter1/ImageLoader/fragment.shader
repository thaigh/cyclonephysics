﻿#version 330 core

// Inputs from the vertex shader
smooth in vec2 vUV; // 2D texture coordinates

// Outputs from fragment shader to geometry shader
layout (location=0) out vec4 vFragColor;

// Uniforms
uniform sampler2D textureMap; // The image to display

void main()
{
    // Sample the textureMap at the given 2D texture coodinates to obtain the colour
    vFragColor = texture(textureMap, vUV);
}