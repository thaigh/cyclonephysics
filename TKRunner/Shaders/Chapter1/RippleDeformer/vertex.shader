﻿#version 330 core

// Attributes
layout(location = 0) in vec3 vVertex;

// Uniforms
uniform mat4 MVP;
uniform float time;

// Constants
const float amplitude = 0.125;
const float frequency = 4;
const float PI = 3.141592;

void main()
{
    // Overwrites GL.Position vector coordinate
    // using Sin Wave
    float nDistance = length(vVertex);
    float y = amplitude * sin(-PI * nDistance * frequency + time);
    gl_Position = MVP * vec4(vVertex.x, y, vVertex.z, 1);
}