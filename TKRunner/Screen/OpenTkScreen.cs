﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace TKRunner.Screen
{
    public class OpenTkScreen : IGameScreen
    {
        public OpenTkScreen () { }

        public void Initialise () { }

        public void Unload() { }

        public void Render (FrameEventArgs e)
        {
            // render graphics
            GL.Clear (ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.MatrixMode (MatrixMode.Projection);
            GL.LoadIdentity ();
            GL.Ortho (-1.0, 1.0, -1.0, 1.0, 0.0, 4.0);

            GL.Begin (PrimitiveType.Triangles);

            GL.Color3 (Color.Red); // (Color.MidnightBlue);
            GL.Vertex2 (-1.0f, 1.0f);
            GL.Color3 (Color.SpringGreen);
            GL.Vertex2 (0.0f, -1.0f);
            GL.Color3 (Color.Ivory);
            GL.Vertex2 (1.0f, 1.0f);

            GL.End ();
        }

        public void Resize (int width, int height)
        {
            GL.Viewport (0, 0, width, height);
        }

        public void UpdateFrame(FrameEventArgs e)
        {
            
        }
    }
}

