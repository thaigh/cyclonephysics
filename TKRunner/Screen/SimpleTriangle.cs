﻿using System;
using Packt.OpenGL.Cookbook.Infrastructure;
using OpenTK.Graphics.OpenGL4;
using TKRunner.Models;
using OpenTK;
using TKRunner.Screen;

namespace TKRunner.Screen
{
    public class SimpleTriangle : IGameScreen
    {

        private readonly GLSLShaderProgram _shader = new GLSLShaderProgram();

        private const int NumberOfVertices = 3;
        private readonly Vertex[] _vertices = new Vertex[NumberOfVertices];
        private readonly short[] _indices = new short[NumberOfVertices];

        private int _vaoId;
        private VertexBufferObject<Vertex> _verticesVbo;
        private VertexBufferObject<short> _indexVbo;

        private Matrix4 P = Matrix4.Identity;
        private Matrix4 MV = Matrix4.Identity;

        private const string VertexAttribute = "vVertex";
        private const string ColourAttribute = "vColor";
        private const string MvpUniform = "MVP";


        #region IGameScreen

        public void Initialise() {
            CreateShaders ();
            CreateVertices ();
            CreateVaosVbos ();
        }

        public void Resize(int width, int height) {
            GL.Viewport (0, 0, width, height);
            //P = Matrix4.CreateOrthographic(-1, 1, -1, 1);
            P = Matrix4.CreateOrthographic (-2, 2, -1, 1);
        }

        public void Unload()
        {
            _shader.Delete();

            _verticesVbo.UnloadBuffer();
            _indexVbo.UnloadBuffer();

            GL.DeleteVertexArrays(1, ref _vaoId);
        }

        public void Render(FrameEventArgs e) {
            // Clear colour and depth buffer
            GL.Clear (ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            _shader.Use (); // Bind the shader
            {
                Matrix4 mvp = P * MV;
                GL.UniformMatrix4(_shader.GetUniform(MvpUniform), false, ref mvp);
                GL.DrawElements(PrimitiveType.Triangles, NumberOfVertices, DrawElementsType.UnsignedShort, 0);
            }
            _shader.Unuse(); // Unbind shader
        }

        public void UpdateFrame(FrameEventArgs e)
        {

        }

        #endregion

        private void CreateShaders() {
            _shader.LoadFromFile(ShaderType.VertexShader,  "Shaders/Chapter1/SimpleTriangle/vertex.shader");
            _shader.LoadFromFile(ShaderType.FragmentShader,"Shaders/Chapter1/SimpleTriangle/fragment.shader");

            _shader.CreateAndLink();

            _shader.Use();
            {
                _shader.AddAttribute(VertexAttribute);
                _shader.AddAttribute(ColourAttribute);
                _shader.AddUniform(MvpUniform);
            }
            _shader.Unuse();
        }

        /// <summary>
        /// Create the geometry and topology. We will store the attributes together in
        /// an interleaved vertex format, that is, we will store the vertex attributes
        /// in a struct containing two attributes, position and color.
        /// </summary>
        private void CreateVertices() {

            _vertices[0].Color= new Vector3(1,0,0);
            _vertices[1].Color= new Vector3(0,1,0);
            _vertices[2].Color= new Vector3(0,0,1);

            _vertices[0].Position= new Vector3(-1,-1,0);
            _vertices[1].Position= new Vector3(0,1,0);
            _vertices[2].Position= new Vector3(1,-1,0);

            _indices[0] = 0;
            _indices[1] = 1;
            _indices[2] = 2;
        }

        #region RegisterGeometry

        private void CreateVaosVbos() {
            GenBuffers ();
            RegisterGeometry();
        }

        private void GenBuffers()
        {
            GL.GenVertexArrays(1, out _vaoId);

            _verticesVbo = new VertexBufferObject<Vertex>(BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw, _vertices);
            _indexVbo = new VertexBufferObject<short>(BufferTarget.ElementArrayBuffer, BufferUsageHint.StaticDraw, _indices);
        }

        private void RegisterGeometry() {
            GL.BindVertexArray(_vaoId);
            {
                RegisterVertexData();
                RegisterIndexData();
            }
        }

        private void RegisterVertexData()
        {
            _verticesVbo.BufferData();

            _verticesVbo.BufferVertexAttribPointerData(new VertexAttributeDetails
            {
                AttributeId = _shader.GetAttribute(VertexAttribute),
                AttributeSize = VertexUtils.DimensionalityOfVector3,
                AttributeType = VertexAttribPointerType.Float,
                IsNormalised = false,
                Stride = VertexUtils.SizeOfVertex,
                OffsetToAttributeData = VertexUtils.PositionOffset
            });

            _verticesVbo.BufferVertexAttribPointerData(new VertexAttributeDetails
            {
                AttributeId = _shader.GetAttribute(ColourAttribute),
                AttributeSize = VertexUtils.DimensionalityOfVector3,
                AttributeType = VertexAttribPointerType.Float,
                IsNormalised = false,
                Stride = VertexUtils.SizeOfVertex,
                OffsetToAttributeData = VertexUtils.ColourOffset
            });
        }

        private void RegisterIndexData() {
            _indexVbo.BufferData();
        }

        #endregion

    }
}

