﻿using System;
using OpenTK;

namespace TKRunner.Screen
{
    public interface IGameScreen
    {
        void Initialise ();
        void Render (FrameEventArgs e);
        void Resize (int width, int height);
        void Unload();
        void UpdateFrame(FrameEventArgs e);
    }
}

