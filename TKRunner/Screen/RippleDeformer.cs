﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;
using Packt.OpenGL.Cookbook.Infrastructure;
using TKRunner.Models;

namespace TKRunner.Screen
{
    class RippleDeformer : IGameScreen
    {
        private readonly GLSLShaderProgram _shader = new GLSLShaderProgram();

        private const string VertexAttrib = "vVertex";
        private const string MvpUniform = "MVP";
        private const string TimeUniform = "time";

        // Number of Quads on Axis
        private const int NumX = 40;
        private const int NumZ = 40;

        // Size of plane in world space
        private const int SizeX = 4;
        private const int SizeZ = 4;
        private const float HalfSizeX = SizeX / 2.0f;
        private const float HalfSizeZ = SizeZ / 2.0f;

        // Ripple displacement speed
        private const int Speed = 2;

        private const int TotalIndices = NumX * NumZ * 2 * 3;
        private Vector3[] _vertices = new Vector3[(NumX + 1) * (NumZ + 1)];
        private ushort[] _indices = new ushort[TotalIndices];

        private int _vaoId;
        private VertexBufferObject<Vector3> _vertexVbo;
        private VertexBufferObject<ushort> _indexVbo;

        private Matrix4 P = Matrix4.Identity;
        private float _totalElapsedTime = 0;

        private float _rotX = 0f, _rotY = 0f, _rotZ = 0;
        private float _posX = 0f, _posY = 0f, _posZ = -7f;

        #region IGameScreen

        public void Initialise()
        {
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);

            RegisterShaders();
            DefineGeometry();
            GenerateBuffers();
            RegisterGeometry();
        }

        public void Resize(int width, int height)
        {
            //P = Matrix4.CreateOrthographic(-1, 1, -1, 1);
            const float FOV = 45.0f;
            float fovy = MathHelper.DegreesToRadians(FOV);
            P = Matrix4.CreatePerspectiveFieldOfView(fovy, width / (float)height, 0.1f, 1000.0f);
        }

        public void Unload()
        {
            _shader.Delete();

            _vertexVbo.UnloadBuffer();
            _indexVbo.UnloadBuffer();

            GL.DeleteVertexArrays(1, ref _vaoId);
        }

        public void UpdateFrame(FrameEventArgs e)
        {
            Console.Clear();
            Console.WriteLine("Position: (x: {0}  y: {1}  z:  {2})", _posX, _posY, _posZ);
            Console.WriteLine("Rotation: (x: {0}  y: {1}  z:  {2})", _rotX, _rotY, _rotZ);

            ResolveKeyboard();
        }

        private void ResolveKeyboard()
        {
            KeyboardState kbd = Keyboard.GetState();

            const float smallChange = 0.01f;

            if (kbd.IsKeyDown(Key.Up)) _rotX = Math.Min(2, _rotX + smallChange);
            if (kbd.IsKeyDown(Key.Down)) _rotX = Math.Max(-2, _rotX - smallChange);
            if (kbd.IsKeyDown(Key.Left)) _rotY += smallChange;
            if (kbd.IsKeyDown(Key.Right)) _rotY -= smallChange;
        }

        #endregion

        #region RenderDetails

        public void Render(FrameEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            //GL.ClearColor(Color.Red);

            float phaseTime = GetPhaseTime(e.Time);

            // Could be around the wrong way...
            Matrix4 mvp = BuildMvpLeftHand();
            //Matrix4 mvp = BuildMvpRightHand();

            _shader.Use();
            {
                // glUniformMatrix4fv(shader("MVP"), 1, GL_FALSE, glm::value_ptr(MVP));
                GL.UniformMatrix4(_shader.GetUniform(MvpUniform), false, ref mvp);

                // glUniform1f(shader("time"), time);
                GL.Uniform1(_shader.GetUniform(TimeUniform), phaseTime);

                // glDrawElements(GL_TRIANGLES, TOTAL_INDICES, GL_UNSIGNED_SHORT, 0);
                GL.DrawElements(BeginMode.Triangles, TotalIndices, DrawElementsType.UnsignedShort, 0);
            }
            _shader.Unuse();
        }

        private float GetPhaseTime(double deltaT)
        {
            _totalElapsedTime += (float)deltaT;

            float phaseTime = _totalElapsedTime * Speed;
            return phaseTime;
        }


        private Matrix4 BuildMvpLeftHand()
        {
            // Left Hand System

            bool rightHanded = true;

            Vector3 vec = new Vector3(_posX, _posY, _posZ);
            Matrix4 translation = Matrix4.CreateTranslation(vec);

            // Rotations seem to be right-handed
            Matrix4 rotationX = Matrix4.CreateRotationX(_rotX);
            Matrix4 rotationY = Matrix4.CreateRotationY(_rotY);

            Matrix4 rotation = rightHanded
                ? rotationY * rotationX
                : rotationX * rotationY;

            // Also right-handed??
            Matrix4 mv = rightHanded
                ? rotation * translation
                : translation * rotation;

            // So whatever this is, it isn't right...
            Matrix4 mvp = rightHanded
                ? mv * P
                : P * mv;

            return mvp;
        }

        /*
        private Matrix4 BuildMvpRightHand()
        {
            // Right Hand System
            Vector3 vec = new Vector3(0, -1, Dist);
            Matrix4 translation = Matrix4.CreateTranslation(vec);

            // Create rotation matrix
            Matrix4 rotationX = Matrix4.CreateRotationX(RotX);
            Matrix4 rotationY = Matrix4.CreateRotationY(RotY);
            Matrix4 rotation = rotationY * rotationX;

            Matrix4 mv = rotation * translation;

            Matrix4 mvp = P * mv;
            return mvp;
        }
        */

        #endregion

        private void RegisterShaders()
        {
            // Load Shader programs
            _shader.LoadFromFile(ShaderType.VertexShader, "Shaders/Chapter1/RippleDeformer/vertex.shader");
            _shader.LoadFromFile(ShaderType.FragmentShader, "Shaders/Chapter1/RippleDeformer/fragment.shader");
            _shader.CreateAndLink();

            // Register attributes
            _shader.Use();
            {
                _shader.AddAttribute(VertexAttrib);
                _shader.AddUniform(MvpUniform);
                _shader.AddUniform(TimeUniform);
            }
            _shader.Unuse();
        }

        #region DefineGeometry


        private void DefineGeometry()
        {
            DefineVertices();
            DefineIndices();
        }

        private void DefineVertices()
        {
            //setup plane vertices
            int vertexCount = 0;
            for (int i = 0; i <= NumZ; i++)
            {
                for (int j = 0; j <= NumX; j++)
                {
                    _vertices[vertexCount++] = new Vector3(
                        (((float)j / (NumX - 1)) * 2 - 1) * HalfSizeX,
                        0,
                        (((float)i / (NumZ - 1)) * 2 - 1) * HalfSizeZ);
                }
            }
        }

        private void DefineIndices()
        {
            // Indices
            int indexCount = 0;
            for (int i = 0; i < NumZ; i++)
            {
                for (int j = 0; j < NumX; j++)
                {
                    int i0 = i * (NumX + 1) + j;
                    int i1 = i0 + 1;
                    int i2 = i0 + (NumX + 1);
                    int i3 = i2 + 1;

                    if ((i + j) % 2 == 1)
                    {
                        _indices[indexCount++] = (ushort)i0;
                        _indices[indexCount++] = (ushort)i2;
                        _indices[indexCount++] = (ushort)i1;

                        _indices[indexCount++] = (ushort)i1;
                        _indices[indexCount++] = (ushort)i2;
                        _indices[indexCount++] = (ushort)i3;
                    }
                    else
                    {
                        _indices[indexCount++] = (ushort)i0;
                        _indices[indexCount++] = (ushort)i2;
                        _indices[indexCount++] = (ushort)i3;

                        _indices[indexCount++] = (ushort)i0;
                        _indices[indexCount++] = (ushort)i3;
                        _indices[indexCount++] = (ushort)i1;
                    }
                }
            }
        }

        #endregion


        #region DefineBuffers

        private void GenerateBuffers()
        {
            GL.GenVertexArrays(1, out _vaoId);

            _vertexVbo = new VertexBufferObject<Vector3>(BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw, _vertices);
            _indexVbo = new VertexBufferObject<ushort>(BufferTarget.ElementArrayBuffer, BufferUsageHint.StaticDraw, _indices);
        }

        private void RegisterGeometry()
        {
            GL.BindVertexArray(_vaoId);
            {
                BindVertexData();
                BindIndexData();
            }
        }

        private void BindVertexData()
        {
            _vertexVbo.BufferData();

            _vertexVbo.BufferVertexAttribPointerData(new VertexAttributeDetails
            {
                AttributeId = _shader.GetAttribute(VertexAttrib),
                AttributeSize = VertexUtils.DimensionalityOfVector3,
                AttributeType = VertexAttribPointerType.Float,
                IsNormalised = false,
                Stride = 0,
                OffsetToAttributeData = VertexUtils.PositionOffset
            });
        }


        private void BindIndexData()
        {
            _indexVbo.BufferData();
        }

        #endregion

    }
}
