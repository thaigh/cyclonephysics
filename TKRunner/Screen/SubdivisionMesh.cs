﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using Packt.OpenGL.Cookbook.Infrastructure;
using System.Runtime.InteropServices;
using TKRunner.Models;
using OpenTK.Input;

namespace TKRunner.Screen
{
    public class SubdivisionMesh : IGameScreen
    {

        GLSLShaderProgram _shader = new GLSLShaderProgram();
        private const string VertexAttribute = "vVertex";
        private const string MvpUniform = "MVP";
        private const string SubDivisionsUniform = "sub_divisions";


        private int NumberOfSubdivisions = 5;

        private const int NumVertices = 4;
        private const int NumIndices = 6;
        private Vector3[] _vertices = new Vector3[NumVertices]; // A Square
        private short[] _indices = new short[NumIndices]; // Two Triangles that form a square


        private int _vaoId;
        private VertexBufferObject<Vector3> _verticesVbo;
        private VertexBufferObject<short> _indicesVbo;

        private float _posX = 0, _posY = 0, _posZ = -35;
        private float _rotX = 0, _rotY = 0, _rotZ = 0;

        private Matrix4 P = Matrix4.Identity;

        #region IGameScreen

        public void Initialise()
        {
            RegisterShaders();
            DefineGeometry();
            DefineBuffers();
        }

        public void Render(FrameEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            Matrix4 mv = GetMV();

            _shader.Use();
            {
                GL.Uniform1(_shader.GetUniform(SubDivisionsUniform), NumberOfSubdivisions);

                // Translate matrix
                Matrix4 translate = Matrix4.CreateTranslation(-5, 0, -5);
                mv = translate * mv;
                Matrix4 mvp = mv * P;

                GL.UniformMatrix4(_shader.GetUniform(MvpUniform), false, ref mvp);
                GL.DrawElements(BeginMode.Triangles, 6, DrawElementsType.UnsignedShort, 0);

                // Translate matrix
                translate = Matrix4.CreateTranslation(10, 0, 0);
                mv = translate * mv;
                mvp = mv * P;

                GL.UniformMatrix4(_shader.GetUniform(MvpUniform), false, ref mvp);
                GL.DrawElements(BeginMode.Triangles, 6, DrawElementsType.UnsignedShort, 0);

                // Translate matrix
                translate = Matrix4.CreateTranslation(0, 0, 10);
                mv = translate * mv;
                mvp = mv * P;

                GL.UniformMatrix4(_shader.GetUniform(MvpUniform), false, ref mvp);
                GL.DrawElements(BeginMode.Triangles, 6, DrawElementsType.UnsignedShort, 0);

                // Translate matrix
                translate = Matrix4.CreateTranslation(-10, 0, 0);
                mv = translate * mv;
                mvp = mv * P;

                GL.UniformMatrix4(_shader.GetUniform(MvpUniform), false, ref mvp);
                GL.DrawElements(BeginMode.Triangles, 6, DrawElementsType.UnsignedShort, 0);
            }
            _shader.Unuse();

        }

        private Matrix4 GetMV()
        {

            // Get translation matrix
            Matrix4 translation = Matrix4.CreateTranslation(_posX, _posY, _posZ);

            // Get rotation matrix
            Matrix4 rotationX = Matrix4.CreateRotationX(_rotX);
            Matrix4 rotationY = Matrix4.CreateRotationY(_rotY);
            Matrix4 rotation = rotationY * rotationX;

            Matrix4 mv = rotation * translation;

            return mv;
        }


        public void Resize(int width, int height)
        {
            const float FOV = 45.0f;
            float fovy = MathHelper.DegreesToRadians(FOV);
            P = Matrix4.CreatePerspectiveFieldOfView(fovy, width / (float)height, 0.01f, 1000.0f);
        }

        public void Unload()
        {
            _shader.Delete();

            _verticesVbo.UnloadBuffer();
            _indicesVbo.UnloadBuffer();

            GL.DeleteVertexArrays(1, ref _vaoId);
        }

        public void UpdateFrame(FrameEventArgs e)
        {
            Console.Clear();
            Console.WriteLine("Position: (x: {0}  y: {1}  z:  {2})", _posX, _posY, _posZ);
            Console.WriteLine("Rotation: (x: {0}  y: {1}  z:  {2})", _rotX, _rotY, _rotZ);
            Console.WriteLine("Number of Subdivisions: {0})", NumberOfSubdivisions);

            ResolveKeyboard();
        }

        private void ResolveKeyboard()
        {
            KeyboardState kbd = Keyboard.GetState();

            const float smallChange = 0.01f;

            if (kbd.IsKeyDown(Key.Comma))  NumberOfSubdivisions = BoundInt(1, 8, NumberOfSubdivisions + 1);
            if (kbd.IsKeyDown(Key.Period)) NumberOfSubdivisions = BoundInt(1, 8, NumberOfSubdivisions - 1);
            if (kbd.IsKeyDown(Key.Up))    _rotX = Math.Min(2,  _rotX + smallChange);
            if (kbd.IsKeyDown(Key.Down))  _rotX = Math.Max(-2, _rotX - smallChange);
            if (kbd.IsKeyDown(Key.Left))  _rotY += smallChange;
            if (kbd.IsKeyDown(Key.Right)) _rotY -= smallChange;
        }

        private int BoundInt(int min, int max, int val)
        {
            return Math.Min(
                max,
                Math.Max(min, val)
            );
        }

        #endregion


        private void RegisterShaders()
        {
            _shader.LoadFromFile(ShaderType.VertexShader, "Shaders/Chapter1/SubdivisionMesh/vertex.shader");
            _shader.LoadFromFile(ShaderType.GeometryShader, "Shaders/Chapter1/SubdivisionMesh/geom.shader");
            _shader.LoadFromFile(ShaderType.FragmentShader, "Shaders/Chapter1/SubdivisionMesh/fragment.shader");

            _shader.CreateAndLink();

            _shader.Use();
            {
                _shader.AddAttribute(VertexAttribute);
                _shader.AddUniform(MvpUniform);
                _shader.AddUniform(SubDivisionsUniform);

                GL.Uniform1(_shader.GetUniform(SubDivisionsUniform), NumberOfSubdivisions);
            }
            _shader.Unuse();
        }

        #region DefineGeometry

        private void DefineGeometry()
        {
            DefineVertices();
            DefineIndices();
        }

        private void DefineVertices() {
            _vertices[0] = new Vector3(-5, 0, -5);
            _vertices[1] = new Vector3(-5, 0, 5);
            _vertices[2] = new Vector3(5, 0, 5);
            _vertices[3] = new Vector3(5, 0, -5);
        }

        private void DefineIndices() {
            _indices[0] = 0; _indices[1] = 1; _indices[2] = 2;
            _indices[3] = 0; _indices[4] = 2; _indices[5] = 3;
        }


        #endregion


        #region DefineBuffers

        private void DefineBuffers()
        {
            GenerateBuffers();
            RegisterGeometry();

            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
        }

        public void GenerateBuffers()
        {
            GL.GenVertexArrays(1, out _vaoId);

            _verticesVbo = new VertexBufferObject<Vector3>(BufferTarget.ArrayBuffer, BufferUsageHint.StaticDraw, _vertices);
            _indicesVbo = new VertexBufferObject<short>(BufferTarget.ElementArrayBuffer, BufferUsageHint.StaticDraw, _indices);
        }

        public void RegisterGeometry()
        {
            GL.BindVertexArray(_vaoId);
            {
                BindVertices();
                BindIndices();
            }
        }

        private void BindVertices() {

            _verticesVbo.BufferData();

            _verticesVbo.BufferVertexAttribPointerData(new VertexAttributeDetails
            {
                AttributeId = _shader.GetAttribute(VertexAttribute),
                AttributeSize = VertexUtils.DimensionalityOfVector3,
                AttributeType = VertexAttribPointerType.Float,
                IsNormalised = false,
                Stride = 0,
                OffsetToAttributeData = 0
            });

        }

        private void BindIndices() {
            _indicesVbo.BufferData();
        }


        #endregion

    }
}
