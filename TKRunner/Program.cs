﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;
using System.Drawing;
using OpenTK.Graphics;
using TKRunner.Screen;

namespace TKRunner
{
    class MainClass
    {
        private static GameWindow game;
        private static IGameScreen screen;

        [STAThread]
        public static void Main (string[] args)
        {
            
            game = new GameWindow(800, 600, GraphicsMode.Default, "TK-Runner", GameWindowFlags.Default,
                DisplayDevice.Default, 3, 3,
                GraphicsContextFlags.ForwardCompatible | GraphicsContextFlags.Debug);

            game.Load += GameLoad;
            game.Resize += GameResize;
            game.UpdateFrame += GameUpdateFrame;
            game.RenderFrame += GameRenderFrame;
            game.Unload += GameUnload;

            //screen = new OpenTkScreen ();
            //screen = new SimpleTriangle();
            //screen = new RippleDeformer();
            screen = new SubdivisionMesh();

            // Run the game at 60 updates per second
            game.Run(60.0);

        }

        private static void GameUnload(object sender, EventArgs e)
        {
            screen.Unload();
        }

        private static void GameRenderFrame (object sender, FrameEventArgs e)
        {
            screen.Render (e);
            game.SwapBuffers ();
        }

        private static void GameUpdateFrame (object sender, FrameEventArgs e)
        {
            if (game.Keyboard [Key.Escape])
                game.Exit ();

            screen.UpdateFrame(e);
        }

        private static void GameResize (object sender, EventArgs e)
        {
            GL.Viewport (0,0, game.Width, game.Height);
            screen.Resize (game.Width, game.Height);
        }

        private static void GameLoad(object sender, EventArgs e)
        {
            game.VSync = VSyncMode.On;
            screen.Initialise ();
        }
    }
}
