﻿using System;
using System.Runtime.InteropServices;
using OpenTK;

namespace TKRunner.Models
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Vertex
    {
        public Vector3 Position;
        public Vector3 Color;
    }

    public static class VertexUtils
    {
        /// <summary>
        /// A vertex is represented in 3D Space (x,y,z)
        /// So the Dimensionality of a Vertex is 3
        /// </summary>
        /// <value>The dimensionality of vertex.</value>
        public static int DimensionalityOfVector3 { get { return 3; }}

        /// <summary>
        /// A Vector3 is represented by 3 Floats (one for each Dimension)
        /// So the size of a single Vector3 is 3xFloat
        /// </summary>
        /// <value>The size of vector3.</value>
        public static int SizeOfVector3 { get { return sizeof(float) * DimensionalityOfVector3; } }

        /// <summary>
        /// Vertexes are comprised of one Vector3 for Position, and one Vector3 for Colour
        /// So the size of a Vertex is the 2xVector3
        /// </summary>
        /// <value>The size of vertex.</value>
        public static int SizeOfVertex { get { return SizeOfVector3 * 2; } }

        /// <summary>
        /// Vertices are layed out sequentially in memory by Position, then Colour
        /// So the offset from the start of the Vertex object to the start of the Position attribute
        /// is 0 bytes
        /// </summary>
        public static int PositionOffset { get { return 0; } }

        /// <summary>
        /// Vertices are layed out sequentially in memory by Position, then Colour
        /// So the offset from the start of the Vertex object to the start of the Colour attribute
        /// is the size of the Position Vector3
        /// </summary>
        public static int ColourOffset { get { return SizeOfVector3; } }
    }
}

